class King extends Piece {
    private int row;
    private int col;
    private int flag ;
    public King(int r,int c, int f){
        col = c ;
        row = r ;
        flag = f;
    }

    @Override
    ArrayList<ArrayList<Integer>> move(ChessBoard cb, int row, int col) {
        
        ArrayList<ArrayList<Integer>>  P_Moves = new  ArrayList<ArrayList<Integer>> ();
        
        ArrayList<Integer> temp = new ArrayList<Integer> () ;
        
        if(col > 1)//chap
        { temp.add(row);
        temp.add(col-1);
        }
        P_Moves.add(temp);
        temp  = new ArrayList<Integer> () ;
        if(col< 8)//rast
        {
            temp.add(row);
            temp.add(col+1);
             P_Moves.add(temp);
        }
        temp  = new ArrayList<Integer> () ;
         if(row > 1)//payin
        { temp.add(row-1);
        temp.add(col);
        P_Moves.add(temp);
        }
        
        temp  = new ArrayList<Integer> () ;
         if(row < 8)//bala
        { temp.add(row+1);
        temp.add(col);
        P_Moves.add(temp);
        }
        
        temp  = new ArrayList<Integer> () ;
         if(col > 1 && row > 1)//junube gharbi
        { temp.add(row-1);
        temp.add(col-1);
        P_Moves.add(temp);
        }
        
        temp  = new ArrayList<Integer> () ;
         if(col <8 && row < 8 ) //shomal shaghi
        { temp.add(row+1 );
        temp.add(col + 1);
        P_Moves.add(temp);
        }
        
        temp  = new ArrayList<Integer> () ;
         if(col > 1 && row < 8) // shomal gharb
        { temp.add(row+1);
        temp.add(col-1);
        P_Moves.add(temp);
        }
        
        temp  = new ArrayList<Integer> () ;
         if(col < 8 && row > 1) // junub shargh
        { temp.add(row -1);
        temp.add(col+1);
        P_Moves.add(temp);
        }
        
         return P_Moves ;
    }

    @Override
    int get_row() {
        return row ;
    }

    @Override
    int get_col() {
       return col ;
    }

    @Override
    int get_flag() {
        return flag ;
    }

}
