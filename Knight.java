class Knight extends Piece{

        private int row ;
        private int col ;
        private int flag ;
        public Knight(int r,int c, int f){
        col = c ;
         row = r ;
        flag = f;
    }
    @Override
    ArrayList<ArrayList<Integer>> move(ChessBoard cb, int row, int col) {
         ArrayList<ArrayList<Integer>>  p_moves = new  ArrayList<ArrayList<Integer>> ();
        
        ArrayList<Integer> temp = new ArrayList<Integer> () ;
        /*
        ...
          .
        */
        if (row + 1 < 9 && col - 2 > 0) {
            temp.add(row + 1);
            temp.add(col-2);
            p_moves.add(temp);            
        }
        temp = new ArrayList<Integer> () ;
        /*
          .
        ...
        */
        if ( row - 1 > 0 && col - 2 > 0){
            temp.add(row - 1);
           temp.add(col-2);
           p_moves.add(temp);            
        }
        temp = new ArrayList<Integer> () ;
        /*
        ..
         .
         .
        */
        if ( row + 2 < 9 && col - 1 > 0 ){
            temp.add(row + 2);
           temp.add(col-1);
           p_moves.add(temp);
        }
        temp = new ArrayList<Integer> () ;
        /*
         .
         .
        ..
        */
        if( row - 2 > 0 && col - 1 > 0){
            temp.add(row -2);
            temp.add(col-1);
            p_moves.add(temp); 
        }
     
        temp = new ArrayList<Integer> () ;
        /*
          .
        ...
        */
        if ( row + 2 < 9 && col + 1 < 9){
            temp.add(row +2);
            temp.add(col+1);
            p_moves.add(temp);
        }

        temp = new ArrayList<Integer> () ;
        /*
        .
        .
        ..
        */
        if( row - 2 > 0 && col + 1 < 9){
            temp.add(row - 2);
            temp.add(col+1);
            p_moves.add(temp); 
        }

        temp = new ArrayList<Integer> () ;
        /*
        .
        ...
        */
        if( row - 1 > 0 && col + 2 < 9){
            temp.add(row - 1);
           temp.add(col + 2);
           p_moves.add(temp);            
        }

        temp = new ArrayList<Integer> () ;
        // ...
        // .
        if(row + 1 < 9 && col + 2 < 9) {
            temp.add(row + 1);
           temp.add(col+ 2);
           p_moves.add(temp);            
        }

      return p_moves ;  
    }

    @Override
    int get_row() {
        return row ;
    }

    @Override
    int get_col() {
        return col ;
    }

    @Override
    int get_flag() {
        return flag ;
    }
    
}
