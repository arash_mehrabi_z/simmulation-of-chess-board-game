# Simmulation of Chess Board Game

In this simulation user tells the available pieces on the board and determines their coordinates.
Then he/she tries to move a piece and then the program will tell if it is a valid move and what will happen next.