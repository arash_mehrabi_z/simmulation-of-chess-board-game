class Bishop extends Piece {
    
    private int row ;
    private int col ;
    private int flag ;
    public Bishop(int r,int c, int f){
        col = c ;
        row = r ;
        flag = f;
    }
    @Override
    ArrayList<ArrayList<Integer>> move(ChessBoard cb, int r, int c) {
        ArrayList<ArrayList<Integer>>  p_moves = new  ArrayList<ArrayList<Integer>> ();
        ArrayList<Integer> temp = new ArrayList<Integer> () ;
        int j =0 ;
        int i =0 ;
        //peymayesh ghotre asli (be baalaa va raast)
        for (i=r+1 , j=c+1 ; i < 9 && j< 9; j++ ,i++) {
           
            temp.add(i);
            temp.add(j);
            if( cb.get_square(i, j) != null) //age be saddi khord, edame nemide.
            {
                p_moves.add(temp);
                break;                
            }
            p_moves.add(temp);
            temp = new ArrayList<Integer> () ;
        }
        //ghotre asli ( be paayin va chap )
        for (i=r-1 , j=c-1 ; i > 0 && j> 0 ;j-- ,i--) {
           
            temp.add(i);
            temp.add(j);
            if( cb.get_square(i, j) != null) //age be saddi khord, edame nemide.
            {
                p_moves.add(temp);
                break;
            }
            p_moves.add(temp);
            temp = new ArrayList<Integer> () ;
        }
        //peymayesh ghotre farei (be baalaa va chap)
        temp = new ArrayList<Integer> () ;
        for (i=r+1 , j=c-1 ; i < 9 && j> 0 ;j--, i++) {
           
            temp.add(i);
            temp.add(j);
            if( cb.get_square(i, j) != null) //age be saddi khord, edame nemide.
            {
                 p_moves.add(temp);   
                 break;
            }
            p_moves.add(temp);  
            temp = new ArrayList<Integer> () ;
        }
         //peymayesh ghotre farei (be paayin va raast)
        temp = new ArrayList<Integer> () ;
        for (i=r-1 , j=c+1 ; i > 0 && j < 9 ;j++, i--) {
           
            temp.add(i);
            temp.add(j);
            if( cb.get_square(i, j) != null) //age be saddi khord, edame nemide.
            {
                p_moves.add(temp);                
                break;                
            }
            p_moves.add(temp);
            temp = new ArrayList<Integer> () ;
        } 
       
          return p_moves ;  
        
    }

    @Override
    int get_row() {
       return row ;
    }

    @Override
    int get_col() {
        return col ;
    }

    @Override
    int get_flag() {
        return flag ;
    }
    
}
