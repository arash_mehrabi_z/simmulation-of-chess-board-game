import java.util.ArrayList;
import java.util.Scanner;


public class Main {
   public static void main(String args[]){
       Scanner in = new Scanner(System.in);
        Piece obj = null; //hame mohre haa ro har baar too in obj zakhire mikonim.
       int flag = 0 ;  //flag hame mohre haa ro too in zakhire mikonim.
       int row=0 ;
       int col=0 ;
       int n = in.nextInt(); //tedad mohre haa
       ChessBoard ch = new ChessBoard ();
       
        for (int i = 0; i < n; i++) { //baraaye gereftane mohree haa
            String s = in.next();     
            if(s.charAt(0) == 'W')
                flag = 1 ;
            else
                flag = 0;
            switch(s.charAt(2)){
                case 'A' :
                   col = 1 ;
                   break;
                case 'B' :
                    col = 2 ;
                    break ;
                case 'C' :
                   col = 3 ;
                   break;
                case 'D' :
                   col = 4 ;
                   break;
                case 'E' :
                   col = 5 ;
                   break;
                case 'F' :
                   col = 6 ;
                   break;
                case 'G' :
                   col = 7 ;
                   break;
                case 'H' :
                   col = 8 ;
                   break;   
            }
            switch(s.charAt(3)){
                case '1' :
                   row = 1 ;
                   break;
                case '2' :
                    row = 2 ;
                    break ;
                case '3' :
                   row = 3 ;
                   break;
                case '4' :
                   row = 4 ;
                   break;
                case '5' :
                   row = 5 ;
                   break;
                case '6' :
                   row = 6 ;
                   break;
                case '7' :
                   row = 7 ;
                   break;
                case '8' :
                   row = 8 ;
                   break;   
            }

            switch(s.charAt(1)){
                case 'K' :
                    obj = new King(row,col,flag);
                    ch.Add(obj);
                    break;
                 case 'Q' :
                    obj = new Queen(row,col,flag);
                    ch.Add(obj);
                    break;    
                      case 'B' :
                    obj = new Bishop(row,col,flag);
                    ch.Add(obj);
                    break;
                 case 'R' :
                    obj = new Rook(row,col,flag);
                    ch.Add(obj);
                    break;    
                     case 'N' :
                    obj = new Knight(row,col,flag);
                    ch.Add(obj);
                    break;
                 case 'P' :
                    obj = new Pawn(row,col,flag);
                    ch.Add(obj);
                    break;    
                  
            }
        }
        
        
          int m = in.nextInt(); //tedade soal haa
          for (int i = 0; i < m; i++) {
            ArrayList<Integer> temp1 = new ArrayList<> () ;
            ArrayList<Integer> temp2 = new ArrayList<> () ;
            String s = in.next();                
            switch(s.charAt(0)){
                case 'A' :
                   col = 1 ;
                   break;
                case 'B' :
                    col = 2 ;
                    break ;
                case 'C' :
                   col = 3 ;
                   break;
                case 'D' :
                   col = 4 ;
                   break;
                case 'E' :
                   col = 5 ;
                   break;
                case 'F' :
                   col = 6 ;
                   break;
                case 'G' :
                   col = 7 ;
                   break;
                case 'H' :
                   col = 8 ;
                   break;   
            }
            switch(s.charAt(1)){
                case '1' :
                   row = 1 ;
                   break;
                case '2' :
                    row = 2 ;
                    break ;
                case '3' :
                   row = 3 ;
                   break;
                case '4' :
                   row = 4 ;
                   break;
                case '5' :
                   row = 5 ;
                   break;
                case '6' :
                   row = 6 ;
                   break;
                case '7' :
                   row = 7 ;
                   break;
                case '8' :
                   row = 8 ;
                   break;   
            }
            
            temp1.add(row);
            temp1.add(col);
           
             switch(s.charAt(2)){
                case 'A' :
                   col = 1 ;
                   break;
                case 'B' :
                    col = 2 ;
                    break ;
                case 'C' :
                   col = 3 ;
                   break;
                case 'D' :
                   col = 4 ;
                   break;
                case 'E' :
                   col = 5 ;
                   break;
                case 'F' :
                   col = 6 ;
                   break;
                case 'G' :
                   col = 7 ;
                   break;
                case 'H' :
                   col = 8 ;
                   break;   
            }
            switch(s.charAt(3)){
                case '1' :
                   row = 1 ;
                   break;
                case '2' :
                    row = 2 ;
                    break ;
                case '3' :
                   row = 3 ;
                   break;
                case '4' :
                   row = 4 ;
                   break;
                case '5' :
                   row = 5 ;
                   break;
                case '6' :
                   row = 6 ;
                   break;
                case '7' :
                   row = 7 ;
                   break;
                case '8' :
                   row = 8 ;
                   break;   
            }      
            temp2.add(row);
            temp2.add(col);
        
            Piece src = ch.get_square(temp1.get(0) , temp1.get(1)) ; //mohre mabda
            Piece dest = ch.get_square(temp2.get(0) , temp2.get(1)) ; //mohre maghsad
            
            if ( ! (src instanceof Pawn)) {
                ArrayList<ArrayList<Integer>> p_moves_src = src.move(ch, src.get_row(),src.get_col()) ; //possible moves of src ;
                if( !p_moves_src.contains(temp2) ) //mitoone bere un khoone aslan?
                    System.out.println("NO") ; 
                else {
                    if ( dest == null) // khoonei ke mikhade bere pore?
                        System.out.println("YES") ;
                    else {
                        if ( src.get_flag() == dest.get_flag() ) //"dest" harife?
                            System.out.println("NO");
                        else 
                            System.out.println("TAKE");
                    }
                }
            }
            
            else { //src ye sarbaaze
                Pawn src_p = (Pawn) ch.get_square(temp1.get(0) , temp1.get(1)) ;
                ArrayList<ArrayList<Integer>> total_moves = src_p.move(ch, src_p.get_row(), src_p.get_col());
                ArrayList<ArrayList<Integer>> take_moves = src_p.take();
                ArrayList<ArrayList<Integer>> simple_moves = src_p.move(ch, src_p.get_row(), src_p.get_col());
                total_moves.addAll(take_moves) ;
               
                 if( !total_moves.contains(temp2) ) //mitoone bere un khoone aslan?
                    System.out.println("NO") ; 
                 else{
                     if( take_moves.contains(temp2) ){ //age jozve harekate take e.
                         if (src_p.get_flag() != dest.get_flag() ) //khooneyi ke mikhad bere bara harife.
                             System.out.println("TAKE") ;
                         else
                             System.out.println("NO") ; //age khodi bashe nemitoone ba take bere aslan.
                     }
                     else{ //age jozve harekate move e.
                         if (dest == null) //age khali bashe mitoone bere.
                             System.out.println("YES") ;
                         else
                             System.out.println("NO") ; //por bashe, nemitoone bere.
                     }
                 }
                
            }

          }
    }
}
