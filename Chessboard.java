class ChessBoard {
    Piece[][] board = new Piece[9][9];
    public void Add(Piece x){
        board[x.get_row()][x.get_col()] = x;
    }
    public Piece get_square(int i, int j){
        return board[i][j] ;
    }
    
}

