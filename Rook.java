class Rook extends Piece {
    private int row ;
    private int col ;
    private int flag ;
    public Rook(int r,int c, int f){
        col = c ;
        row = r ;
        flag = f;
    }
    @Override
   ArrayList<ArrayList<Integer>> move(ChessBoard cb, int r, int c){
      ArrayList<ArrayList<Integer>> p_moves = new ArrayList<ArrayList<Integer>> ();
      ArrayList<Integer> temp = new ArrayList<Integer> () ;
              
              
        for (int i = c; i < 9; i++) {//peymayesh satr (be raast)
           if(i != col){
                temp.add(row);
                temp.add(i);
                if( cb.get_square(row, i) != null){
                    p_moves.add(temp);
                    break;
                }
                p_moves.add(temp);
                temp = new ArrayList<Integer> () ;
           }
        }
        temp = new ArrayList<Integer> () ;
        for (int i = c; i > 0; i--) {//peymayesh satr (be chap)
           if(i != col){
                temp.add(row);
                temp.add(i);
                if( cb.get_square(row, i) != null){
                    p_moves.add(temp);
                    break;
                }
                p_moves.add(temp);
                temp = new ArrayList<Integer> () ;
           }
        }        
        temp = new ArrayList<Integer> () ;
        for (int i = r; i < 9; i++) {//peymayesh sutun (be baalaa)
            if (i != row) {
                 temp.add(i );
                 temp.add(col);
                if( cb.get_square(i, col) != null){
                    p_moves.add(temp);
                    break;
                }                 
                p_moves.add(temp);
                temp = new ArrayList<Integer> () ;
            }
        }
        temp = new ArrayList<Integer> () ;
        for (int i = r; i > 0; i--) {//peymayesh sutun (be paayin)
            if (i != row) {
                 temp.add(i );
                 temp.add(col);
                if( cb.get_square(i, col) != null){
                    p_moves.add(temp);
                    break;
                }                 
                p_moves.add(temp);
                temp = new ArrayList<Integer> () ;
            }
        }
        return p_moves ;
    }

    @Override
    int get_row() {
       return row ;
    }

    @Override
    int get_col() {
       return col ;
    }

    @Override
    int get_flag() {
        return flag ;
    }
}
