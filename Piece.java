abstract class Piece {
   private int row;
    private int col;
    int flag; //White: 1, Black:0
    abstract ArrayList<ArrayList<Integer>> move(ChessBoard cb, int row, int col);  
    abstract int get_row();
    abstract int get_col();
    abstract int get_flag();
      
}   
