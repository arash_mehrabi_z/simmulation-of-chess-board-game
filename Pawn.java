class Pawn extends Piece{
     
    private int row ;
    private int col ;
    private int flag ;
     public Pawn(int r,int c, int f){
        col = c ;
        row = r ;
        flag = f;
    }
    @Override
    ArrayList<ArrayList<Integer>> move(ChessBoard cb, int r, int c) {//khane haye momken baraye harekat
         ArrayList<ArrayList<Integer>>  p_moves = new  ArrayList<ArrayList<Integer>> ();
        
        ArrayList<Integer> temp = new ArrayList<Integer> () ;
        
        if(row == 2 && flag ==1){//harekat aval sarbaz sefid
            if( cb.get_square(row + 1, col) == null) {
                temp.add(row+2);
                temp.add(col);
                p_moves.add(temp);
            }
        temp = new ArrayList<Integer> () ;
        temp.add(row +1);
        temp.add(col);
        p_moves.add(temp);        

        temp = new ArrayList<Integer> () ;
        }
        else if(row == 7 && flag == 0){//harekat aval sarbaz meshki
             temp.add(row -1);
            temp.add(col);
            p_moves.add(temp);
        temp = new ArrayList<Integer> () ;
        if( cb.get_square(row-2, col)!= null) {
            temp.add(row-2);
            temp.add(col);
            p_moves.add(temp);
        }

        temp = new ArrayList<Integer> () ;
        }
        else if(flag == 1)//sarbaz sefid
        {
             temp.add(row +1);
            temp.add(col);
            p_moves.add(temp);
        temp = new ArrayList<Integer> () ;
        
        }
        else if(flag==0){//sarbaz meshki
             temp.add(row -1);
            temp.add(col);
            p_moves.add(temp);
        temp = new ArrayList<Integer> () ;
        }                  
       
        return p_moves ;
    }

    @Override
    int get_row() {
        return row ;
    }

    @Override
    int get_col() {
        return col ;
    }
    
    ArrayList<ArrayList<Integer>> take(){//khane haye momken baraye gereftan 
           //mohrehaye raghib
       
        ArrayList<ArrayList<Integer>>  t_moves = new  ArrayList<ArrayList<Integer>> ();
        ArrayList<Integer> take = new ArrayList<Integer> () ;
        if(flag ==1){//take sarbaz sefid
            if(row != 1 && col != 1){
            take.add(row+1);
            take.add(col-1);
            t_moves.add(take);
            take = new ArrayList<Integer> () ;}
        if(row !=1 && col !=8){
              take.add(row+1);
            take.add(col+1);
            t_moves.add(take);
            take = new ArrayList<Integer> () ;}
        }
        if(flag == 0){//take sarbaz meshki
         if(row != 8 && col != 1){
            take.add(row - 1);
            take.add(col-1);
            t_moves.add(take);
            take = new ArrayList<Integer> () ;}
        if(row !=8 && col !=8){
              take.add(row - 1);
            take.add(col+ 1);
            t_moves.add(take);
            take = new ArrayList<Integer> () ;}
       
    }
         return t_moves ;
       }

    @Override
    int get_flag() {
       return flag ;
    }
}
